const root = document.getElementById('root');
const todoWrap = document.createElement('div');
const input = document.createElement('input');
const list = document.createElement('ul');
let data = [];

document.addEventListener('DOMContentLoaded', DocReady);
list.addEventListener("click", itemClicked, false);

function DocReady() {
    createPage(root)
}

const setItem = (item) => {
    const done = item.done ? 'class="done"' : '';
    return `<li><button type="button" data-value="${item.value}">&times;</button><span ${done} data-value="${item.value}">${item.value}</span></li>`
};

function onKeyPress(press) {
    if (press.keyCode === 13) {
        const value = this.value.trim();
        this.value = '';

        if (value) {
            addItem(value);
            updateStorage()
        }
    }
}

function deleteItem(elem) {
    const li = elem.closest('li');
    li.parentNode.removeChild(li);

    data = data.filter(item => item.value !== elem.dataset.value)
}

function toggleDone(element) {
    element.classList.toggle('done');

    data.forEach((item) => {
        if (item.value === element.dataset.value) {
            item.done = !item.done
        }
    });
}

function itemClicked(e) {
    if (e.target.tagName === 'BUTTON') {
        deleteItem(e.target);
        updateStorage();
    }

    if (e.target.tagName === 'SPAN') {
        toggleDone(e.target);
        updateStorage();
    }
}

function addItem(value) {
    const item = {value, done: false};
    data.push(item);

    list.innerHTML += setItem(item);
}

function updateStorage() {
    localStorage.setItem('todos', JSON.stringify(data))
}

function createPage() {
    const todos = localStorage.getItem('todos');

    if (todos) {
        data = JSON.parse(todos);
        list.innerHTML = data.map(item => setItem(item)).join('');
    }

    todoWrap.className = 'todo';
    input.type = 'text';
    input.setAttribute('placeholder', 'Введите task');

    input.addEventListener('keypress', onKeyPress);
    todoWrap.appendChild(input);
    todoWrap.appendChild(list);

    root.appendChild(todoWrap);
}
